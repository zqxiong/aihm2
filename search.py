# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

#Kent Shikama and Ziqi Xiong


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
from sets import Set
from game import Actions


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"

    #dictionary that keeps track the parent of each state
    parents = dict()
    closed = Set()
    fringe = util.Stack()

    start = (problem.getStartState(),None,0)
    fringe.push(start)
    parents[start] = start

    while not fringe.isEmpty():
        expanded = fringe.pop()
        if problem.isGoalState(expanded[0]):
            return getPath(parents,expanded)
        if not expanded[0] in closed:
            closed.add(expanded[0])
            for successor in problem.getSuccessors(expanded[0]):
                fringe.push(successor)
                parents[successor] = expanded
    return None

# function that takes a parents dictionary, a node and returns the path that leads to
# that node
def getPath(parents,node):
    path = []
    pointer = node
    while parents[pointer] is not pointer:
        path.insert(0,pointer[1])
        pointer = parents[pointer]
    return path

from random import random
def breadthFirstSearch(problem):
        parents = dict()
        closed = Set()
        fringe = util.Queue()

        start = (problem.getStartState(),None,0,random())
        fringe.push(start)
        parents[start] = start

        while not fringe.isEmpty():
            expanded = fringe.pop()
            if problem.isGoalState(expanded[0]):
                return getPath(parents,expanded)
            if not expanded[0] in closed:
                closed.add(expanded[0])
                for successor in problem.getSuccessors(expanded[0]):
                    successor = list(successor)
                    successor.append(random())
                    successor = tuple(successor)
                    fringe.push(successor)
                    parents[successor] = expanded
        return None

def uniformCostSearch(problem):
        parents, costs = dict(), dict()
        closed = Set()
        fringe = util.PriorityQueue()

        start = (problem.getStartState(),None,0)
        costs[start]=0
        fringe.push(start,0)
        parents[start] = start

        while not fringe.isEmpty():
            expanded = fringe.pop()
            if problem.isGoalState(expanded[0]):
                return getPath(parents,expanded)
            if not expanded[0] in closed:
                closed.add(expanded[0])
                for successor in problem.getSuccessors(expanded[0]):
                    if (successor[0] not in closed) and ((successor not in costs) or (successor[2]+costs[expanded] < costs[successor])):
                        costs[successor] = successor[2]+costs[expanded]
                        fringe.push(successor,costs[successor])
                        parents[successor] = expanded
        return None

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    parents, costs = dict(), dict()
    closed = Set()
    fringe = util.PriorityQueue()
    start = (problem.getStartState(),None,0)
    costs[start]=0+heuristic(start[0],problem)
    fringe.push(start,costs[start])
    parents[start] = start
    while not fringe.isEmpty():
        expanded = fringe.pop()
        if problem.isGoalState(expanded[0]):
            return getPath(parents,expanded)
        if not expanded[0] in closed:
            closed.add(expanded[0])
            for successor in problem.getSuccessors(expanded[0]):
                if (successor[0] not in closed) and ((successor not in costs) or (successor[2]+costs[expanded] < costs[successor])):
                    costs[successor] = successor[2]+costs[expanded]+heuristic(successor[0],problem)-heuristic(expanded[0],problem)
                    fringe.push(successor,costs[successor])
                    parents[successor] = expanded
    return None


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
